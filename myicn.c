#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/acpi.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/hrtimer.h>
#include <linux/input/mt.h>


#define TS_I2C_BUS 3
#define TS_I2C_ADDR 0x30
#define TS_IRQ 0x44
#define DRIVER_VERSION "0.9.0"
#define CTP_NAME "chipone-ts"
#define ICN85XX_PROG_IIC_ADDR (0x30)

#define SCREEN_MAX_X             (1366)
#define SCREEN_MAX_Y             (768)

#define POINT_NUM                   5
#define POINT_SIZE 7

#define ICN85XX_WITHOUT_FLASH           0x11
#define ICN85XX_WITH_FLASH              0x22

#define IIC_RETRY_NUM 3



#define CTP_POLL_TIMER              (16)
#define CTP_START_TIMER             (100)




typedef struct _POINT_INFO
{
    unsigned char  u8ID;
    unsigned short u16PosX;
    unsigned short u16PosY;
    unsigned char  u8Pressure;
    unsigned char  u8EventId;
}POINT_INFO;


struct icn85xx_ts_data {
    struct i2c_client        *client;
    struct input_dev         *input_dev;
    struct work_struct       ts_event_work;
    struct workqueue_struct  *ts_workqueue;
    struct hrtimer           timer;
    int         ictype;
    int         code_loaded_flag; 
    POINT_INFO  point_info[POINT_NUM+1];
    int         point_num;
    int         irq;
    int         irq_is_disable;
    int         use_irq;
    int         work_mode;
    int         screen_max_x;
    int         screen_max_y;
    int         revert_x_flag;
    int         revert_y_flag;
    int         exchange_x_y_flag;
};


#define COL_NUM                         24
#define ROW_NUM                         36


static struct i2c_client *this_client;
short log_rawdata[COL_NUM][ROW_NUM] = {{0}};
short log_diffdata[COL_NUM][ROW_NUM] = {{0}};


typedef enum
{
    R_OK = 100,
    R_FILE_ERR,
    R_STATE_ERR,
    R_ERASE_ERR,
    R_PROGRAM_ERR,
    R_VERIFY_ERR,
}E_UPGRADE_ERR_TYPE;


#define B_SIZE 32

struct file *fp;
int g_status = R_OK;
static int fw_size = 0;
static unsigned char *fw_buf;
static char boot_mode = ICN85XX_WITHOUT_FLASH;
static char firmware[128] = {"icnfirmware.sys"};


#define CTP_REPORT_PROTOCOL 1


int icn85xx_i2c_rxdata(unsigned short addr, char *rxdata, int length){
    int ret = -1;
    int retries = 0;
    unsigned char tmp_buf[2];
    struct i2c_msg msgs[] = {
        {
            .addr   = this_client->addr,
            .flags  = 0,
            .len    = 2,
            .buf    = tmp_buf,
        },
        {
            .addr   = this_client->addr,
            .flags  = I2C_M_RD,
            .len    = length,
            .buf    = rxdata,
        },
    };
    tmp_buf[0] = (unsigned char)(addr>>8);
    tmp_buf[1] = (unsigned char)(addr);
    while(retries < IIC_RETRY_NUM){
        ret = i2c_transfer(this_client->adapter, msgs, 2);
        if(ret == 2)break;
        retries++;
    }
    if (retries >= IIC_RETRY_NUM)
	printk("from %s: i2c read error: %d\n", __func__, ret);
    return ret;

}

int icn85xx_i2c_txdata(unsigned short addr, char *txdata, int length)
{
    int ret = -1;
    unsigned char tmp_buf[128];
    int retries = 0;
    struct i2c_msg msg[] = {
        {
            .addr   = this_client->addr,
            .flags  = 0,
            .len    = length + 2,
            .buf    = tmp_buf,
        },
    };
    if (length > 125)
    {
        printk("\n%s too big datalen = %d!\n", __func__, length);
        return -1;
    }
    tmp_buf[0] = (unsigned char)(addr>>8);
    tmp_buf[1] = (unsigned char)(addr);

    if (length != 0 && txdata != NULL)
    {
        memcpy(&tmp_buf[2], txdata, length);
    }   
    
    while(retries < IIC_RETRY_NUM)
    {
        ret = i2c_transfer(this_client->adapter, msg, 1);
        if(ret == 1)break;
        retries++;
    }

    if (retries >= IIC_RETRY_NUM)
    {
        printk("\n%s i2c write error: %d\n", __func__, ret);
    }

    return ret;
}

int icn85xx_write_reg(unsigned short addr, char para)
{
    char buf[3];
    int ret = -1;

    buf[0] = para;
    ret = icn85xx_i2c_txdata(addr, buf, 1);
    if (ret < 0) {
        printk("\nwrite reg failed! %#x ret: %d\n", buf[0], ret);
        return -1;
    }

    return ret;
}


int icn85xx_read_reg(unsigned short addr, char *pdata)
{
    int ret = -1;
    ret = icn85xx_i2c_rxdata(addr, pdata, 1);
    return ret;
}

int  icn85xx_open_fw( char *fw)
{
    int file_size;
    mm_segment_t fs;
    struct inode *inode = NULL; 
    fp = filp_open(fw, O_RDONLY, 0);
    if (IS_ERR(fp)) {
        printk("\nread fw file error\n");
        return -1;
    }

    inode = fp->f_path.dentry->d_inode;
    file_size = inode->i_size;
    fs = get_fs();
    set_fs(KERNEL_DS);

    return  file_size;
}


int  icn85xx_close_fw(void)
{
        filp_close(fp, NULL);
    return 0;
}

int icn85xx_prog_i2c_txdata(unsigned int addr, char *txdata, int length){
    int ret = -1;
    char tmp_buf[128];
    int retries = 0;
    struct i2c_msg msg[] = {
        {
            .addr   = ICN85XX_PROG_IIC_ADDR,
            .flags  = 0,
            .len    = length + 3,
            .buf    = tmp_buf,
        },
    };

    if (length > 125)
    {
        printk("/n%s too big datalen = %d!\n", __func__, length);
        return -1;
    }
    tmp_buf[0] = (unsigned char)(addr>>16);
    tmp_buf[1] = (unsigned char)(addr>>8);
    tmp_buf[2] = (unsigned char)(addr);


    if (length != 0 && txdata != NULL)
    {
        memcpy(&tmp_buf[3], txdata, length);
    }

    while(retries < IIC_RETRY_NUM)
    {
        ret = i2c_transfer(this_client->adapter, msg, 1);
        if(ret == 1)break;
        retries++;
    }

    if (retries >= IIC_RETRY_NUM)
    {
        printk("/n%s i2c write error: %d\n", __func__, ret);
    }
    return ret;
}

int icn85xx_prog_i2c_rxdata(unsigned int addr, char *rxdata, int length)
{
    int ret = -1;
    int retries = 0;



    unsigned char tmp_buf[3];
    struct i2c_msg msgs[] = {
        {
            .addr   = ICN85XX_PROG_IIC_ADDR,
            .flags  = 0,
            .len    = 3,
            .buf    = tmp_buf,
        },
        {
            .addr   = ICN85XX_PROG_IIC_ADDR,
            .flags  = I2C_M_RD,
            .len    = length,
            .buf    = rxdata,
        },
    };

    tmp_buf[0] = (unsigned char)(addr>>16);
    tmp_buf[1] = (unsigned char)(addr>>8);
    tmp_buf[2] = (unsigned char)(addr);

    while(retries < IIC_RETRY_NUM)
    {
        ret = i2c_transfer(this_client->adapter, msgs, 2);
        if(ret == 2)break;
        retries++;
    }

    if (retries >= IIC_RETRY_NUM)
    {
        printk("\n%s i2c read error: %d\n", __func__, ret);
    }
    return ret;
}

int icn85xx_check_progmod(void)
{
    int ret;
    unsigned char ucTemp = 0x0;
    ret = icn85xx_prog_i2c_rxdata(0x040002, &ucTemp, 1);
    if(ret < 0)
    {
        printk("\nicn85xx_check_progmod error, ret: %d\n", ret);
        return ret;
    }
    if(ucTemp == 0x85)
        return 0;
    else
        return -1;

}

int icn85xx_goto_progmode(void)
{
    int ret = -1;
    int retry = 3;
    unsigned char ucTemp;
    while(retry > 0)
    {
        ucTemp = 0x5a;
        ret = icn85xx_prog_i2c_txdata(0xcc3355, &ucTemp,1);
        mdelay(2);
        ucTemp = 01;
        ret = icn85xx_prog_i2c_txdata(0x040400, &ucTemp,1);
        mdelay(2);
        ret = icn85xx_check_progmod();
        if(ret == 0)
            return ret;
        retry--;
        mdelay(2);
    }
    if(retry == 0)
        return -1;

    return 0;
}



int icn85xx_update_status(int status){
    g_status = status;
    return 0;
}

int icn85xx_get_status(void){
    return  g_status;
}


static unsigned int crc32table[256] = {    
 0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,    
 0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,    
 0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,    
 0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,    
 0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,    
 0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,    
 0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,    
 0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,    
 0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,    
 0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,    
 0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,    
 0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,    
 0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,    
 0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,    
 0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,    
 0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,    
 0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,    
 0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,    
 0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,    
 0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,    
 0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,    
 0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,    
 0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,    
 0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,    
 0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,    
 0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,    
 0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,    
 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,    
 0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,    
 0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,    
 0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,    
 0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,    
 0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,    
 0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,    
 0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,    
 0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,    
 0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,    
 0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,    
 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,    
 0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,    
 0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,    
 0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,    
 0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};

unsigned int icn85xx_crc_calc(unsigned crc_in, char *buf, int len)  
{
    int i;
    unsigned int crc = crc_in;
    for(i = 0; i < len; i++)
        crc = (crc << 8) ^ crc32table[((crc >> 24) ^ *buf++) & 0xFF];
    return crc;
}


int  icn85xx_crc_enable(unsigned char enable){
    unsigned char ucTemp;
    int ret = 0;
    if(enable==1)
    {
        ucTemp = 1;
        ret = icn85xx_prog_i2c_txdata(0x40028, &ucTemp, 1 );
    }
    else if(enable==0)
    {
        ucTemp = 0;
        ret = icn85xx_prog_i2c_txdata(0x40028, &ucTemp, 1 );
    } 
    return ret;
}

int  icn85xx_crc_check(unsigned int crc, unsigned int len){
    int ret;
    unsigned int crc_len;
    unsigned int crc_result;    
    unsigned char ucTemp[4] = {0,0,0,0};     
    ret= icn85xx_prog_i2c_rxdata(0x4002c, ucTemp, 4 );
    crc_result = ucTemp[3]<<24 | ucTemp[2]<<16 | ucTemp[1] << 8 | ucTemp[0];
    ret = icn85xx_prog_i2c_rxdata(0x40034, ucTemp, 2);
    crc_len = ucTemp[1] << 8 | ucTemp[0];

    if((crc_result == crc) && (crc_len == len))
        return 0;
    else
    {
        printk("\ncrc_fw: 0x%x\n", crc);
        printk("\ncrc_result: 0x%x\n", crc_result);
        printk("\ncrc_len: %d\n", crc_len);
        return -1;
    }
}


int  icn85xx_read_fw(int offset, int length, char *buf){

    loff_t  pos = offset;
        vfs_read(fp, buf, length, &pos); 
    return 0;
}


int  icn85xx_fw_download(unsigned int offset, unsigned char * buffer, unsigned int size)
{
    int i;
#ifdef ENABLE_BYTE_CHECK
    char testb[B_SIZE];
#endif

    icn85xx_prog_i2c_txdata(offset,buffer,size);
#ifdef ENABLE_BYTE_CHECK
    icn85xx_prog_i2c_rxdata(offset,testb,size);
    for(i = 0; i < size; i++)
    {
        if(buffer[i] != testb[i])
        {
            printk("\nbuffer[%d]:%x  testb[%d]:%x\n",i,buffer[i],i,testb[i]);
            return -1;
        }
    }
#endif
    return 0;
}




int  icn85xx_bootfrom_sram(void){
    int ret = -1;
    unsigned char ucTemp = 0x03;
    unsigned long addr = 0x40400;
    printk("\nicn85xx_bootfrom_sram\n");
    ret = icn85xx_prog_i2c_txdata(addr, &ucTemp, 1 );
    return ret;
}



E_UPGRADE_ERR_TYPE  icn85xx_fw_update(char *fw){
    int file_size, last_length;
    int j, num;
    char temp_buf[B_SIZE];
    unsigned int crc_fw;

    file_size = icn85xx_open_fw(fw);
    if(file_size < 0)
    {
        icn85xx_update_status(R_FILE_ERR);
        return R_FILE_ERR;
    }
    if(icn85xx_goto_progmode() != 0)
    {
       printk("\n icnfwupdatefunc: icn85xx_goto_progmode() != 0 error\n");
        return R_STATE_ERR;
    }
    msleep(1);
    icn85xx_crc_enable(1);

    num = file_size/B_SIZE;
    crc_fw = 0;
    for(j=0; j < num; j++)
    {
        icn85xx_read_fw(j*B_SIZE, B_SIZE, temp_buf);
        crc_fw = icn85xx_crc_calc(crc_fw, temp_buf, B_SIZE);
        if(icn85xx_fw_download(j*B_SIZE, temp_buf, B_SIZE) != 0)
        {
            printk("\nerror j:%d\n",j);
            icn85xx_update_status(R_PROGRAM_ERR);
            icn85xx_close_fw();
            return R_PROGRAM_ERR;
        }
        icn85xx_update_status(5+(int)(60*j/num));
    }
    last_length = file_size - B_SIZE*j;
    if(last_length > 0)
    {
        icn85xx_read_fw(j*B_SIZE, last_length, temp_buf);
        crc_fw = icn85xx_crc_calc(crc_fw, temp_buf, last_length);
        if(icn85xx_fw_download(j*B_SIZE, temp_buf, last_length) != 0)
        {
            printk("\nerror last length\n");
            icn85xx_update_status(R_PROGRAM_ERR);
            icn85xx_close_fw();
            return R_PROGRAM_ERR;
        }
    }
    icn85xx_close_fw();
    icn85xx_update_status(65);
    icn85xx_crc_enable(0);
    if(icn85xx_crc_check(crc_fw, file_size) != 0){
        printk("\ndownload fw error, crc error\n");
        return R_PROGRAM_ERR;
    }
    else
        printk("download fw ok, crc ok\n");

    icn85xx_update_status(70);

    if(ICN85XX_WITHOUT_FLASH == boot_mode)
    {
        if(icn85xx_bootfrom_sram() == 0)
        {
            printk("\nicn85xx_bootfrom_sram error\n");
            icn85xx_update_status(R_STATE_ERR);
            return R_STATE_ERR;
        }
    }
    msleep(50);
    icn85xx_update_status(R_OK);
    printk("upgrade ok\n");
    return R_OK;
}


static int icn85xx_request_input_dev(struct icn85xx_ts_data *icn85xx_ts)
{
    int ret = -1;
    struct input_dev *input_dev;

    input_dev = input_allocate_device();
    if (!input_dev) {
        printk("\nfailed to allocate input device\n");
        return -ENOMEM;
    }
    icn85xx_ts->input_dev = input_dev;

    icn85xx_ts->input_dev->evbit[0] = BIT_MASK(EV_SYN) | BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS) ;
#if CTP_REPORT_PROTOCOL
    __set_bit(INPUT_PROP_DIRECT, icn85xx_ts->input_dev->propbit);
    input_mt_init_slots(icn85xx_ts->input_dev, 5,0);
#else
    set_bit(ABS_MT_TOUCH_MAJOR, icn85xx_ts->input_dev->absbit);
    set_bit(ABS_MT_POSITION_X, icn85xx_ts->input_dev->absbit);
    set_bit(ABS_MT_POSITION_Y, icn85xx_ts->input_dev->absbit);
    set_bit(ABS_MT_WIDTH_MAJOR, icn85xx_ts->input_dev->absbit);
#endif
    input_set_abs_params(icn85xx_ts->input_dev, ABS_MT_POSITION_X, 0, SCREEN_MAX_X, 0, 0);
    input_set_abs_params(icn85xx_ts->input_dev, ABS_MT_POSITION_Y, 0, SCREEN_MAX_Y, 0, 0);
    input_set_abs_params(icn85xx_ts->input_dev, ABS_MT_WIDTH_MAJOR, 0, 255, 0, 0);
    input_set_abs_params(icn85xx_ts->input_dev, ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0);
    input_set_abs_params(icn85xx_ts->input_dev, ABS_MT_TRACKING_ID, 0, 255, 0, 0);

    __set_bit(KEY_MENU,  input_dev->keybit);
    __set_bit(KEY_BACK,  input_dev->keybit);
    __set_bit(KEY_HOME,  input_dev->keybit);
    __set_bit(KEY_SEARCH,  input_dev->keybit);

    input_dev->name = "mouse";
    ret = input_register_device(input_dev);
    if (ret) {
        printk("\nRegister %s input device failed\n", input_dev->name);
        input_free_device(input_dev);
        return -ENODEV;
    }
    printk("\nRegistered %s input device \n", input_dev->name);

    return 0;
}


static enum hrtimer_restart chipone_timer_func(struct hrtimer *timer)
{
    struct icn85xx_ts_data *icn85xx_ts = container_of(timer, struct icn85xx_ts_data, timer);
    queue_work(icn85xx_ts->ts_workqueue, &icn85xx_ts->ts_event_work);
    if(icn85xx_ts->use_irq == 1)
    {
        if((icn85xx_ts->work_mode == 1) || (icn85xx_ts->work_mode == 2))
        {
            hrtimer_start(&icn85xx_ts->timer, ktime_set(CTP_POLL_TIMER/1000, (CTP_POLL_TIMER%1000)*1000000), HRTIMER_MODE_REL);
        }
    }
    else
    {
        hrtimer_start(&icn85xx_ts->timer, ktime_set(CTP_POLL_TIMER/1000, (CTP_POLL_TIMER%1000)*1000000), HRTIMER_MODE_REL);
    }
    return HRTIMER_NORESTART;
}



void icn85xx_rawdatadump(short *mem, int size, char br)
{
    int i;
    for(i=0;i<size; i++)
    {
        if((i!=0)&&(i%br == 0))
            printk("\n");
        printk(" %5d", mem[i]);
    }
    printk("\n");
}




int  icn85xx_log(char diff)
{
    char row = 0;
    char column = 0;
    int i, j, ret;
    char retvalue;
    icn85xx_read_reg(0x8004, &row);
    icn85xx_read_reg(0x8005, &column);
    icn85xx_write_reg(4, 0x20);
    mdelay(1);
    icn85xx_read_reg(2, &retvalue);
    while(retvalue != 1)
    {
        mdelay(1);
        icn85xx_read_reg(2, &retvalue);
    }

    if(diff == 1)
    {
        for(i=0; i<row; i++)
        {
            ret = icn85xx_i2c_rxdata(0x3000 + i*(COL_NUM+2)*2 + 2, &log_diffdata[i][0], column*2);
            if (ret < 0) {
                printk("\n%s read_data i2c_rxdata failed: %d\n", __func__, ret);
            }
            icn85xx_rawdatadump(&log_diffdata[i][0], column, COL_NUM);

        }

    }
    else
    {
        for(i=0; i<row; i++)
        {
            ret = icn85xx_i2c_rxdata(0x2000 + i*COL_NUM*2, &log_rawdata[i][0], column*2);
            if (ret < 0) {
                printk("\n%s read_data i2c_rxdata failed: %d\n", __func__, ret);
            }
            icn85xx_rawdatadump(&log_rawdata[i][0], column, COL_NUM);

        }

    }

    icn85xx_write_reg(2, 0x0);
    icn85xx_write_reg(4, 0x21);
}


static void icn85xx_get_mtpoint(void)
{
    struct icn85xx_ts_data *icn85xx_ts = i2c_get_clientdata(this_client);
    unsigned char buf[POINT_NUM*POINT_SIZE+3]={0};
    static unsigned char finger_last[POINT_NUM + 1]={0};
    unsigned char  finger_current[POINT_NUM + 1] = {0};
    unsigned int position = 0;
    int temp = 0;
    int ret = -1;

    ret = icn85xx_i2c_rxdata(0x1000, buf, POINT_NUM*POINT_SIZE+2);
    if (ret < 0) {
        printk("\n%s read_data i2c_rxdata failed: %d\n", __func__, ret);
        return ret;
    }

    icn85xx_ts->point_num = buf[1];
    if(icn85xx_ts->point_num > 0)
    {
        for(position = 0; position<icn85xx_ts->point_num; position++)
        {
            temp = buf[2 + POINT_SIZE*position] + 1;
            finger_current[temp] = 1;
            icn85xx_ts->point_info[temp].u8ID =0; 
            icn85xx_ts->point_info[temp].u16PosX = (buf[4 + POINT_SIZE*position]<<8) + buf[3 + POINT_SIZE*position];
            icn85xx_ts->point_info[temp].u16PosY = (buf[6 + POINT_SIZE*position]<<8) + buf[5 + POINT_SIZE*position];
            icn85xx_ts->point_info[temp].u8Pressure = buf[7 + POINT_SIZE*position];
            icn85xx_ts->point_info[temp].u8EventId = buf[8 + POINT_SIZE*position];
            if(icn85xx_ts->point_info[temp].u8EventId == 4)
                finger_current[temp] = 0;
            if(1 == icn85xx_ts->revert_x_flag)
            {
                icn85xx_ts->point_info[temp].u16PosX = icn85xx_ts->screen_max_x- icn85xx_ts->point_info[temp].u16PosX;
            }
            if(1 == icn85xx_ts->revert_y_flag)
            {
                icn85xx_ts->point_info[temp].u16PosY = icn85xx_ts->screen_max_y- icn85xx_ts->point_info[temp].u16PosY;
            }
        }
    }
    else
    {
        for(position = 1; position < POINT_NUM+1; position++)
        {
            finger_current[position] = 0;
        }
    }

    for(position = 1; position < POINT_NUM + 1; position++)
    {
        if((finger_current[position] == 0) && (finger_last[position] != 0))
        {
            input_mt_slot(icn85xx_ts->input_dev, position-1);
            input_mt_report_slot_state(icn85xx_ts->input_dev, MT_TOOL_FINGER, false);
        }
        else if(finger_current[position])
        {
            input_mt_slot(icn85xx_ts->input_dev, position-1);
            input_mt_report_slot_state(icn85xx_ts->input_dev, MT_TOOL_FINGER, true);
            input_report_abs(icn85xx_ts->input_dev, ABS_MT_TOUCH_MAJOR, 1);
            input_report_abs(icn85xx_ts->input_dev, ABS_MT_PRESSURE, 200);
            input_report_abs(icn85xx_ts->input_dev, ABS_MT_POSITION_X, icn85xx_ts->point_info[position].u16PosX);
            input_report_abs(icn85xx_ts->input_dev, ABS_MT_POSITION_Y, icn85xx_ts->point_info[position].u16PosY);
        }

    }
    input_sync(icn85xx_ts->input_dev);

    for(position = 1; position < POINT_NUM + 1; position++)
    {
        finger_last[position] = finger_current[position];
    }

}

static void icn85xx_ts_work(struct work_struct *work){
    int ret = -1;
    struct icn85xx_ts_data *icn85xx_ts = i2c_get_clientdata(this_client);  
      
    if(icn85xx_ts->work_mode == 0)
    {
        icn85xx_get_mtpoint();

    }
    else if(icn85xx_ts->work_mode == 1)
    {
        printk("\nraw data\n");
        icn85xx_log(0);
    }
    else if(icn85xx_ts->work_mode == 2)
    {
        printk("\ndiff data\n");
        icn85xx_log(1);
    }
    else if(icn85xx_ts->work_mode == 3)
    {
        printk("\nreinit tp\n");
        icn85xx_write_reg(0, 1);
        mdelay(100);
        icn85xx_write_reg(0, 0);
        icn85xx_ts->work_mode = 0;
    }

}


static int icn85xx_ts_probe(struct i2c_client *client, const struct i2c_device_id *id){
    struct icn85xx_ts_data *icn85xx_ts;
    short fwVersion = 0;
    short curVersion = 0;
    int err = 0;
    int retry;

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
        printk("I2C check functionality failed from %s.\n",__func__);
        return -ENODEV;
    }

    icn85xx_ts = kzalloc(sizeof(*icn85xx_ts), GFP_KERNEL);
    if (!icn85xx_ts)
    {
        printk("Alloc icn85xx_ts memory failed. %s\n",__func__);
        return -ENOMEM;
    }
    memset(icn85xx_ts, 0, sizeof(*icn85xx_ts));
    this_client = client;
    this_client->addr = client->addr;
    this_client->irq = client->irq;
    this_client->dev = client->dev;
    i2c_set_clientdata(client, icn85xx_ts);
    icn85xx_ts->work_mode = 0;
    err= icn85xx_request_input_dev(icn85xx_ts);
    if (err < 0){
        printk("\nrequest input dev failed\n");
        kfree(icn85xx_ts);
        return err;
    }


     if(R_OK == icn85xx_fw_update(firmware)){
            icn85xx_ts->code_loaded_flag = 1;
            printk("\nICN85XX_WITHOUT_FLASH, update OK\n");

     }
     else{
            icn85xx_ts->code_loaded_flag = 0;
            printk("\nICN85XX_WITHOUT_FLASH, update error\n");
            return -1;
     }


        icn85xx_ts->use_irq = 0;
        hrtimer_init(&icn85xx_ts->timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
        icn85xx_ts->timer.function = chipone_timer_func;
        hrtimer_start(&icn85xx_ts->timer, ktime_set(CTP_START_TIMER/1000, (CTP_START_TIMER%1000)*1000000), HRTIMER_MODE_REL);


    INIT_WORK(&icn85xx_ts->ts_event_work, icn85xx_ts_work);
    icn85xx_ts->ts_workqueue = create_singlethread_workqueue(dev_name(&client->dev));
    if (!icn85xx_ts->ts_workqueue) {
        printk("\ncreate_singlethread_workqueue failed.\n");
        kfree(icn85xx_ts);
        return -ESRCH;
    }


return 0;
}


static int  icn85xx_ts_remove(struct i2c_client *client){
	
	struct icn85xx_ts_data *icn85xx_ts = i2c_get_clientdata(client);  
	input_unregister_device(icn85xx_ts->input_dev);
	input_free_device(icn85xx_ts->input_dev);
        hrtimer_cancel(&icn85xx_ts->timer);
	cancel_work_sync(&icn85xx_ts->ts_event_work);
	destroy_workqueue(icn85xx_ts->ts_workqueue);
	kfree(icn85xx_ts);
	i2c_set_clientdata(client, NULL);

	return 0;
}



static const struct i2c_device_id icn85xx_ts_id[] = {
    { CTP_NAME, 0 },
    {}
};

MODULE_DEVICE_TABLE(i2c, icn85xx_ts_id);

static struct i2c_driver icn85xx_ts_driver = {
    .class      = I2C_CLASS_HWMON,
    .probe      = icn85xx_ts_probe,
    .remove     = icn85xx_ts_remove,
    .id_table   = icn85xx_ts_id,
    .driver = {
        .name   = CTP_NAME,
        .owner  = THIS_MODULE,
    },
};

static struct i2c_board_info ts_info = {
    .type	= CTP_NAME,
    .addr	=  TS_I2C_ADDR,
    .irq	=0x44,
};


static int __init ts_init(void){
    struct i2c_adapter *adap = i2c_get_adapter(TS_I2C_BUS);
    i2c_new_device(adap, &ts_info);
    int ret = -1;
    ret = i2c_add_driver(&icn85xx_ts_driver);
return 0;
}


static void __exit ts_exit(void){
    i2c_del_driver(&icn85xx_ts_driver);
    i2c_unregister_device(this_client);

}


module_init(ts_init);
module_exit(ts_exit);
MODULE_DESCRIPTION("MyICN8528 touchscreen controller driver");
MODULE_AUTHOR("Serge Kolotylo sergk.admin@gmail.com>");
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");